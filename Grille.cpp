#include "Grille.h"

Grille::Grille(){
	m_t_grille.loadFromFile("image/grille.png");
	m_s_grille.setTexture(m_t_grille);
	m_s_grille.setPosition(32*5, 32*3);
	for(int i = 0; i < 81; i++)
	{
		m_tableauDeCube[i].setId(CNULL);	
		//std::cout << "ici: " << m_tableauDeCube[i].getId() << '\n';
	}
	for(int i = 0; i < 81; i++)
	{
		m_tableauDeCube[i].setisOnCursor(false);	
		//std::cout << "ici: " << m_tableauDeCube[i].getId() << '\n';
	}
}

void Grille::draw(sf::RenderWindow &window){
	window.draw(m_s_grille);
	m_cursor.draw(window);
	for(int i = 0; i < 81; i++)
	{
		//std::cout << "ici: " << i << '\n';
		if(m_tableauDeCube[i].getId() != CNULL)
		{
			m_tableauDeCube[i].draw(window);
		}
	}
}

void Grille::setAfter(Struct_Cubeid s_cubeid){
	for(int i = 0; i < 81; i++)
	{
		m_tableauDeCube[i].setisOnCursor(false);	
	}
	m_cursor.setAfter(getCubeVide(), getCubeVide(), getCubeVide(), s_cubeid);
}
//faire tomber les blocks
void Grille::falling(){
	for(int n = 0; n < 81; n++){
		if(m_tableauDeCube[n].isOnCursor() == true){continue;}
		if(m_tableauDeCube[n].getId() == CNULL){continue;}
		
	}
}

void Grille::destroy(){
	const int c_posx = m_s_grille.getPosition().x;
	const int c_posy = m_s_grille.getPosition().y;
	int posx = c_posx;
	int posy = c_posy;
	int tmpcouleur = CNULL;
	int nbrcolor = 0;
	int tab[6];
	//std::cout << c_posy <<"\n";

	//line
	std::cout << "on commence a verifier la ligne\n";
	while(posy <= c_posy + (32 * (13 - 1)))//boucles des lignes vvvvv
	{
		//std::cout << "ligne \n";
		tmpcouleur = CNULL;
		while(posx <= c_posx + (32 * (6 - 1)))//boucle des cubes -->
		{
			//std::cout << "cube \n";
			for(int n = 0; n < 81; n++){
				if(m_tableauDeCube[n].isOnCursor() == true){continue;}
				//if(m_tableauDeCube[n].getId() == CNULL){continue;}

				sf::Vector2f point = sf::Vector2f(posx, posy);
				if (m_tableauDeCube[n].getSprite().getGlobalBounds().contains(point))
				{
					// les cubes dans la lignes:
					
					if(tmpcouleur == CNULL || tmpcouleur != m_tableauDeCube[n].getId())
					{
						tab[nbrcolor + 1] = 4242;
						if(nbrcolor >= 3){
							std::cout << "ON ECLATTTTTE\n";
							for(int i = 0; tab[i] != 4242; i++)
							{
								m_tableauDeCube[tab[i]].setId(CNULL);
							}
							this->falling();
						}
						tmpcouleur = m_tableauDeCube[n].getId();
						nbrcolor = 0;
						tab[0] = n;
					}
					if(tmpcouleur == m_tableauDeCube[n].getId()){
						nbrcolor++;					
						tab[nbrcolor] = n;
					}
					//std::cout << "block ici:" << posx << " " << posy << '\n';
					std::cout << "nbrcolorsuite:" << nbrcolor << '\n';
					

					//FIIIIN
				}
			}

			posx = posx + 32;
		}
		posx = c_posx;




		posy = posy + 32;
	}
}

//PROTECTED
Cube *Grille::getCubeVide(){
	for(int i = 0; i < 81; i++)
	{
		//std::cout << "ici: " << i << '\n';
		if(m_tableauDeCube[i].getId() == CNULL)
		{
			m_tableauDeCube[i].setId(1);
			return &m_tableauDeCube[i];
		}
	}
	return NULL;
}

// EVENEMENT
void Grille::onkeypush(){
	m_cursor.onkeypush();
}
int Grille::onGravity(){//collision avec les block du bas et le sol
	for(int i = 0; i < 81; i++)
	{
		if(m_tableauDeCube[i].isOnCursor() == true){
			if(m_tableauDeCube[i].getPosition().y >= 480)
			{
				return 42;
			}
			for(int n = 0; n < 81; n++){
				if(m_tableauDeCube[n].isOnCursor() == true){continue;}
				sf::FloatRect tmpRect = m_tableauDeCube[i].getSprite().getGlobalBounds();
				tmpRect.top = tmpRect.top + 32;
				if(m_tableauDeCube[n].getSprite().getGlobalBounds().intersects(tmpRect)){
					//std::cout << "COLLISION";
					return 42;
				}
			}

		}
	}	
	m_cursor.onGravity();
	return 0;
}

void Grille::onDown(){
}

void Grille::onLeft(){
	for(int i = 0; i < 81; i++)
	{
		if(m_tableauDeCube[i].isOnCursor() == false){continue;}//que le cursorblock

		for(int n = 0; n < 81; n++){
			if(m_tableauDeCube[n].isOnCursor() == true){continue;}//que les blockcarte
			sf::FloatRect tmpRect = m_tableauDeCube[i].getSprite().getGlobalBounds();

			tmpRect.left = tmpRect.left - 32;
			if(m_tableauDeCube[n].getSprite().getGlobalBounds().intersects(tmpRect)){
				return;
			}
		}
	}
	m_cursor.onLeft();
}

void Grille::onRight(){
	for(int i = 0; i < 81; i++)
	{
		if(m_tableauDeCube[i].isOnCursor() == false){continue;}//que le cursorblock

		for(int n = 0; n < 81; n++){
			if(m_tableauDeCube[n].isOnCursor() == true){continue;}//que les blockcarte
			sf::FloatRect tmpRect = m_tableauDeCube[i].getSprite().getGlobalBounds();

			tmpRect.left = tmpRect.left + 32;
			if(m_tableauDeCube[n].getSprite().getGlobalBounds().intersects(tmpRect)){
				return;
			}
		}
	}
	m_cursor.onRight();
}
